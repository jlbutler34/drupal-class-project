<?php

	function spring_preprocess_page(&$variables)
	{
		/*
		"English" version

		check if both sidebars are occupied,
		if so,set them both to 25% 
		set content to 50%

		check if sidebar_right is occupied and sidebar_left not
		if so, content to 75% and right sidebar 25%

		3/8 In-class example

		if sidebar_left is occupied and sidebar_right is occupied
		{
			add class of one-fourth to sidebar_left
			add class of one-fourth to sidebar_right
			add class of one-half to content
		}
		else if sidebar_left is occupied and sidebar_right is not occupied
		{
			add class of one-fourth to sidebar_left
			add class of three-fourths to content
			add class of '' to sidebar_right
		}
		else if sidebar_right is occupied and sidebar_left is not occupied
		{
			add class of one-fourth to sidebar_right
			add class of three-fourths to content
			add class of '' to sidebar_left
		}
		else
		{
			add class full-width to content
			add class of '' to sidebar_left
			add class of '' to sidebar_right
		}

		*/

		$page = $variables['page'];
		$sidebar_left_class = 'one-fourth';
		$sidebar_right_class = 'one-fourth';
		$content_class = 'one-half';
		$variables['page']['tpl_control'] = [];



		if(
			(empty($page['sidebar_left']))
			 &&
			(empty($page['sidebar_right']))
		)
		{
			$sidebar_left_class = '';
			$sidebar_right_class = '';
			$content_class = 'full-width';
		}
		elseif (empty($page['sidebar_left'])
			    &&
			   (!empty($page['sidebar_right']))) 
		{
			$sidebar_left_class = '';
			$sidebar_right_class = 'one-fourth';
			$content_class = 'three-fourths';
		}
		elseif (empty($page['sidebar_right'])
			    &&
			   (!empty($page['sidebar_left'])))
		{
			$sidebar_left_class = 'one-fourth';
			$sidebar_right_class = '';
			$content_class = 'three-fourths';
		}
		else
		{
			$sidebar_left_class = 'one-fourth';
			$sidebar_right_class = 'one-fourth';
			$content_class = 'one-half';
		}

		$variables['page']['tpl_control']['sidebar_left_class'] = $sidebar_left_class;
		$variables['page']['tpl_control']['sidebar_right_class'] = $sidebar_right_class;
		$variables['page']['tpl_control']['content_class'] = $content_class;
	
	}// end spring_preprocess_page function